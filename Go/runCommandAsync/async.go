package main

import (
    "fmt"
    "os/exec"
	"sync"
	"strings"
	"bufio"

)


func main()  {
	cmdName := "ls"
	cmd := exec.Command(cmdName, cmdArgs...)
	cmdReader, _ := cmd.StdoutPipe()
	scanner := bufio.NewScanner(cmdReader)
	go func() {
		for scanner.Scan() {
			fmt.Printf(scanner.Text())
		}
	}()
	cmd.Start()
	err = cmd.Wait()
}