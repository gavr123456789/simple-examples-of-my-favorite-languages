Copying the file directly (without buffer):

import os
copyfile("input.txt", "output.txt")
Buffer for the entire file:

let x = readFile("input.txt")
writeFile("output.txt", x)
Line by line:

var
  i = open("input.txt")
  o = open("output.txt", fmWrite)
 
for line in i.lines:
  o.writeln(line)
 
i.close()
o.close()
With a fixed sized buffer:

const size = 4096
 
var
  i = open("input.txt")
  o = open("output.txt", fmWrite)
  buf: array[size, char]
 
while i.readBuffer(buf.addr, size) > 0:
  discard o.writeBuffer(buf.addr, size)
 
i.close()
o.close()
Using memory mapping:

import memfiles
 
var
  i = memfiles.open("input.txt")
  o = system.open("output.txt", fmWrite)
 
var written = o.writeBuffer(i.mem, i.size)
assert(written == i.size)
 
i.close()
o.close()