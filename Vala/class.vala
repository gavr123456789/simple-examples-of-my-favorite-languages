
class A {  public int a; public int a2; }
class B : A {public int a; }

void foo(B b){
    b.a = 3;   // accesses field B.a
    b.a2 = 4;  // accesses field A.a2
    b.base.a = 5; // accesses field A.a
}

class Sample {
	void run(){
		stdout.printf("Hello, World\n");
	}
	static int main(string[] args){
		var sample = new Sample();
		sample.run();
		return 0;
	}
}
