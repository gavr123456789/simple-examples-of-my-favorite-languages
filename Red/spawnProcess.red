Red [Title: "Simple Spawning Processes line script"]
out: ""
; Non-blocking
call/output "pwd" out prin out 

; Blocking (waits for the process to finish)
call/wait/output "ls" out prin out
