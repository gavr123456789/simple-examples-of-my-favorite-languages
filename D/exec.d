import std.process; 
import std.stdio; 

void main(string[] args) {
    auto ls = executeShell("ls");
    if (ls.status != 0) writeln("Failed");
    else writeln(ls.output);
}