# vala  

## Hello World
A very simple Hello World program:

```vala
void main () {
    print ("hello, world\n");
}
```
### Compile and Run
```
$ vala hello.vala
```
If the binary should have a different name:
```vala
$ valac hello.vala -o greeting
$ ./greeting
```
## Reading User Input
```vala
void main () {
    stdout.printf ("Please enter your name: ");
    string name = stdin.read_line ();
    if (name != null) {
        stdout.printf ("Hello, %s!\n", name);
    }
}
```
Vala provides the objects stdin (standard input), stdout (standard output) and stderr (standard error) for the three standard streams. The printf method takes a format string and a variable number of arguments as parameters.

## Mathematics
Math functions are inside the Math namespace.

```vala
void main () {

    stdout.printf ("Please enter the radius of a circle: ");
    double radius = double.parse (stdin.read_line ());
    stdout.printf ("Circumference: %g\n", 2 * Math.PI * radius);

    stdout.printf ("sin(pi/2) = %g\n", Math.sin (Math.PI / 2));

    // Random numbers

    stdout.printf ("Today's lottery results:");
    for (int i = 0; i < 6; i++) {
        stdout.printf (" %d", Random.int_range (1, 49));
    }
    stdout.printf ("\n");

    stdout.printf ("Random number between 0 and 1: %g\n", Random.next_double ());
}
```
## Command-Line Arguments and Exit Code
```vala
int main (string[] args) {

    // Output the number of arguments
    stdout.printf ("%d command line argument(s):\n", args.length);

    // Enumerate all command line arguments
    foreach (string arg in args) {
        stdout.printf ("%s\n", arg);
    }

    // Exit code (0: success, 1: failure)
    return 0;
}
```
The first command line argument (args[0]) is always the invocation of the program itself.

## Reading and Writing Text File Content
This is very basic text file handling. For advanced I/O you should use GIO's powerful stream classes.

```vala
void main () {
    try {
        string filename = "data.txt";

        // Writing
        string content = "hello, world";
        FileUtils.set_contents (filename, content);

        // Reading
        string read;
        FileUtils.get_contents (filename, out read);

        stdout.printf ("The content of file '%s' is:\n%s\n", filename, read);
    } catch (FileError e) {
        stderr.printf ("%s\n", e.message);
    }
}
```
## Spawning Processes
```vala
void main () {
    try {
        // Non-blocking
        Process.spawn_command_line_async ("ls");

        // Blocking (waits for the process to finish)
        Process.spawn_command_line_sync ("ls");

        // Blocking with output
        string standard_output, standard_error;
        int exit_status;
        Process.spawn_command_line_sync ("ls", out standard_output,
                                               out standard_error,
                                               out exit_status);
    } catch (SpawnError e) {
        stderr.printf ("%s\n", e.message);
    }
}
```
## First Class
```vala
/* class derived from GObject */
public class BasicSample : Object {

    /* public instance method */
    public void run () {
        stdout.printf ("Hello World\n");
    }
}

// application entry point 
int main (string[] args) {
    // instantiate this class, assigning the instance to
    // a type-inferred variable
    var sample = new BasicSample ();
    // call the run method
    sample.run ();
    // return from this main method
    return 0;
}
```
The entry point may as well be inside the class, if you prefer it this way:

```vala
public class BasicSample : Object {

    public void run () {
        stdout.printf ("Hello World\n");
    }

    static int main (string[] args) {
        var sample = new BasicSample ();
        sample.run ();
        return 0;
    }
}
```
In this case main must be declared static.

# kotlin
## What u need to run that examples
```kotlin
import java.io.File
import java.util.*
import  kotlin.coroutines.*
fun main(args: Array<String>) {
//All code here
}
```

## Simple Hello World
```kotlin
    print("Hello World\n")
```
## User Input
```
    var name = readLine()
    println("Hello" + name)
```

## Math
```kotlin
    print ("Please enter the radius of a circle: ")
    var radius:Double =  (readLine()!!).toDouble()

    print ("Circumference: ${2 * Math.PI * radius}\n")
    print ("sin(pi/2) = ${Math.sin (Math.PI / 2)}\n" )
```
### Random numbers
```kotlin
    print ("Today's lottery results:")
    for (i in 0..6) {
        print (" ${(1..49).random()}")
    }

    print ("Random number between 0 and 1: ${(0..1).random()}\n")
```

## Command-Line Arguments and Exit Code
### Output the number of arguments
```kotlin
    print ("${args.size} command line argument(s):\n")
```  
### Enumerate all command line arguments
```kotlin
    for (arg in args) {
        print (arg + "\n")
    }
```
## Reading and Writing Text File Content

### Writing
```kotlin
    var filename = "data.txt";
    var content = "hello, world"
    File(filename).writeText(content)
```

### Reading
```kotlin
    var read:String
    read = File(filename).toString()
    print(read)
```
## Spawning Processes
### Blocking (waits for the process to finish)
```kotlin
    val proc = Runtime.getRuntime().exec("ls")
    Scanner(proc.inputStream).use {
        while (it.hasNextLine()) println(it.nextLine())
    }
```

## Simple class
```kotlin
class Base (){
    fun run(x:String){
        println (x)
    }

    fun add(x:Int, y:Int):Int {
        return x+y
    }
}

    var a = Base()
    val sum = a.add(1,5)
    a.run(sum.toString())
```
 # nim
 # red
 # crystal
 # go